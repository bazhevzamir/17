﻿#include <iostream>

class Vector
{
 private:

    double x = 0;
    double y = 0;
    double z = 0;

 public:
    Vector() : x(5), y(5), z(5) {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z){}

    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }
    double getLenght()
    {
        return std::sqrt (x * x + y * y + z * z);
    }
};

int main()
{
    Vector v;
    v.Show();

    double lenght = v.getLenght();
    std::cout << "\nVector lenght: " << lenght << std::endl;

    return 0;
}
